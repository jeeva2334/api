const demo = require("../models/demoModel")

const hello = ((req,res)=>{
    res.send("Works fine")
})

const hReq = (async(req,res)=>{
    const { name,age } = req.body
    const Demo = new demo({
        name: name,
        age: age
    })
    await Demo.save()
    res.json({message:"Insert Done"});
})
 const hai  = (async(req,res)=> {
    const sample = await demo.find()
    res.send(sample)
 }
 )
 const hmm = (async(req,res)=>{
    const { id } = req.params
    const mmm = await demo.findById({_id:id})
    
    res.send(mmm)
 }
 )

module.exports = {
    hello,
    hReq,
    hai,
    hmm
}