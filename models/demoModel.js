const mongoose = require("mongoose")
const { Schema } = mongoose

const Demo = new Schema({
    name:{
        type: String,
        required: true
    },
    age:{
        type: Number,
        required:true
    }
})

const demo = mongoose.model("Demo",Demo)
module.exports = demo