const express = require("express")
const { hello, hReq,hai,hmm } = require("../controllers/demoController")
const demoRouter = express.Router()

demoRouter.get("/",hello)
demoRouter.post("/",hReq)
demoRouter.get("/getall",hai)
demoRouter.get("/vig/:id",hmm)

module.exports = demoRouter